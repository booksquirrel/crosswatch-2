import * as options from './options_access.js';

const backendInput = document.getElementById("backendUrl");
const defaultRoomInput = document.getElementById("defaultRoomInput");
const defaultNickInput = document.getElementById("defaultNickInput");
const maxMenuSize = 10;
let extensionColor = null;

options.getBackendUrl().then(url => backendInput.value = url);
options.getDefaultRoomId().then(roomId => defaultRoomInput.value = roomId);
options.getDefaultNick().then(nick => defaultNickInput.value = nick);

backendInput.oninput = function () {
  options.setBackendUrl(backendInput.value);
};

defaultRoomInput.oninput = function () {
  options.setDefaultRoomId(defaultRoomInput.value);
};

defaultNickInput.oninput = function () {
  options.setDefaultNick(defaultNickInput.value);
};
