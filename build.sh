#!/bin/bash

mkdir -p build/firefox build/chrome
rsync -av * build/firefox/ --delete --exclude build \
      --exclude manifest-V2.json --exclude manifest-V3.json \
      --exclude options_firefox.js --exclude options_chrome.js \
      --exclude common_firefox.js --exclude common_chrome.js \
      --exclude build.sh \
      --exclude background.js
rsync -av * build/chrome/ --delete --exclude build \
      --exclude manifest-V2.json --exclude manifest-V3.json \
      --exclude options_firefox.js --exclude options_chrome.js \
      --exclude common_firefox.js --exclude common_chrome.js \
      --exclude build.sh \
      --exclude background.html \
      --exclude background.js
cp manifest-V2.json build/firefox/manifest.json
cp manifest-V3.json build/chrome/manifest.json
sed 's/browserAPI/chrome/g' options_access.js > build/chrome/options_access.js
sed 's/browserAPI/browser/g' options_access.js > build/firefox/options_access.js
sed 's/browserAPI/chrome/g' popup_access.js > build/chrome/popup_access.js
sed 's/browserAPI/browser/g' popup_access.js > build/firefox/popup_access.js
sed 's/browserAPI/chrome/g' background.js > build/chrome/background.js
sed 's/browserAPI/browser/g; s/browser.action/browser.browserAction/g' background.js > build/firefox/background.js
