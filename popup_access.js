export function setHasShownReleaseNotes(hasShown) {
  browserAPI.storage.local.set({ hasShownReleaseNotes: hasShown });
}

export function getHasShownReleaseNotes() {
  return browserAPI.storage.local.get({ hasShownReleaseNotes: false }).then(function(data) {
    return data.hasShownReleaseNotes;
  });
}
