import {log} from './common.js';

export function getBackendUrl() {
  return browserAPI.storage.local.get({ backendUrl: "" }).then(function (data) {
    return data.backendUrl;
  });
}

export function getDefaultRoomId() {
  return browserAPI.storage.local.get({ defaultRoomId: "" }).then(function (data) {
    return data.defaultRoomId;
  });
}

export function getDefaultNick() {
  return browserAPI.storage.local.get({ defaultNick: "" }).then(function (data) {
    return data.defaultNick;
  });
}

export function setBackendUrl(url) {
  browserAPI.storage.local.set({ backendUrl: url }).then(function () {
    log("Backend url updated");
  });
}

export function setDefaultRoomId(roomId) {
  browserAPI.storage.local.set({ defaultRoomId: roomId }).then(function () {
    log("Default Room ID updated");
  });
}

export function setDefaultNick(nick) {
  browserAPI.storage.local.set({ defaultNick: nick }).then(function () {
    log("Default Nick updated");
  });
}
