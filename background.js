import {log} from './common.js';
import {setHasShownReleaseNotes} from './popup_access.js';

/* On installation, disable actions for non-relevant tabs,
 * and enable release note in popup. */
browserAPI.runtime.onInstalled.addListener(function (details) {
  // Disable popup action for all tabs by default
  browserAPI.action.disable();

  // If tab is already open with site, enable action for that tab
  browserAPI.tabs.query({
    url: ["https://*.crunchyroll.com/*"]
  }).then(function(result) {
    result.forEach(function(resulttab) {
      browserAPI.action.enable(resulttab.id);
    });
  });

  // Enable release note in popup
  if (details.reason == "install" || details.reason == "update") {
    setHasShownReleaseNotes(false);
  }
});

// When a tab is updated, enable popup if adress is relevant
browserAPI.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  log("chInfo:", changeInfo.status);
  log("tab.url:", tab.url);
  if (changeInfo.status !== undefined) {
    if (tab.url.match(/https:\/\/\w+\.crunchyroll\.com.*/)) {
      browserAPI.action.enable(tabId);
    } else {
      browserAPI.action.disable(tabId);
    }
  }
});
