import {getHasShownReleaseNotes,
        setHasShownReleaseNotes} from './popup_access.js';

const releaseNotesLink = document.getElementById('releaseNotesLink');
const releaseNotes = document.getElementById('releaseNotes');
const releaseOkButton = document.getElementById('releaseOkButton');

function update() {
  getHasShownReleaseNotes().then(hasShown => {
    if (hasShown) {
      releaseNotes.classList.add('noDisplay');
      popupMain.classList.remove('noDisplay');
    } else {
      popupMain.classList.add('noDisplay');
      releaseNotes.classList.remove('noDisplay');
    }
  });
}

releaseNotesLink.onclick = function () {
  setHasShownReleaseNotes(false);
  update();
};

releaseOkButton.onclick = function () {
  setHasShownReleaseNotes(true);
  update();
};

update();
